CREATE DATABASE IF NOT EXISTS docker_data;

CREATE TABLE IF NOT EXISTS docker_data.users(
    id INT,
    name VARCHAR(100)
);

INSERT INTO docker_data.users(id, name)
VALUES (1, 'root');

INSERT INTO docker_data.users(id, name)
VALUES (2, 'Emma');