const mysql = require('mysql2');
const pool = mysql.createConnection({
    // To test database connection locally, use below localhost as host instead of 'mariadb'
    // host: 'localhost',
    host: 'mariadb',
    user: 'root',
    database: 'docker_data',
    password: 'root',
    port: 3306,
});
exports.dbMaria = pool.promise();
