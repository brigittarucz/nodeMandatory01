const app = require('../app');
const chai = require("chai");
const chaiHttp = require("chai-http");
chai.use(chaiHttp);
chai.should();

describe("GET /", () => {
    it('Should get 200', (done) => {
    chai.request(app)
        .get('/')
        .end((err, res) => {
            res.should.have.status(200);
            done();
        })
    }
)})