const assert = require('assert');
const { expect } = require('chai');
const { dbMaria } = require('../database/db_test');

describe('Test database connection', () => {
    it('Should get 1', (done) => {
        dbMaria.execute("SELECT * FROM users WHERE users.name = 'root';")
        .then(res => {
             var user = res[0][0];
            //  console.log(user);
             expect(user.id).to.equal(1);     
             done();            
        })
        .catch(error => console.log(error))
    });
});
