## NodeJS authentication application

This is a very simple nodeJS project made for the purposes of a mandatory assignment in the elective "Development environments" on 6th semester of Web Development.

#### Prerequisites

Clone this repository in a folder called `dockermandatory01`

```
git clone https://gitlab.com/brigittarucz/nodeMandatory01.git
```

#### Installing

Install the neccessary node modules with:

```
npm install
```

#### Run the application

In order to run this project with docker you have to run:

```
docker-compose up --build
```

When it's done building you have to stop the connection with `ctrl + c`

After this, run:

```
docker-compose up
```

And now you're up and running!

Go to `localhost:8000` and you should be met with a website where you can type in a username.

#### Using the application

If you type in a username that doesn't exist in the database, the page will refresh.

If you type in `root`, which is a username, saved in the database, then the page will redirect you to the welcome page.

#### Testing the application

We have created two tests for the application: (1) routing test and (2) database connection test.

The tests successfully run locally when the docker containers are up and running. To run the test, open a new terminal and run the below command.

```
npm test
```

While the routing test (routes.spec.js) does not require any code modification, the database connection test requires a few modifications to correctly run. Modify `host: 'mariadb'` to `host: 'localhost'` within the `db_test.js` and `./controllers/auth.js`. 

This is because we are testing the database outside the Docker container and therefore can only access it through localhost. 

Note that the code to run test as part of CI/CD operation within the `.gitlab-ci.yml` file is commented out. Currently, running application tests as part of CI/CD flags errors but the command is put in place for the future project expensions.

#### Stopping the application

In order to stop the application you run:

```
docker-compose down -v
```

#### Gitlab pages

We've also created a gitlab pages. In order to get this to work we've created an `index.html` page that looks like our project but all functionality must be accessed by running docker.

[Gitlab pages](https://brigittarucz.gitlab.io/nodeMandatory01/)

#### Authors

- Brigitta Rucz
- Hyun Ji Lee
- Michell Aagaard Dranig
