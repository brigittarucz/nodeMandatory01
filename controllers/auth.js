// MARIADB

const mysql = require('mysql2');
const pool = mysql.createConnection({
  // To test database connection locally, use below localhost as host instead of 'mariadb'
  // host: 'localhost',
  host: 'mariadb',
  user: 'root',
  database: 'docker_data',
  password: 'root',
  port: 3306,
});
const dbMaria = pool.promise();

exports.getAuth = (req, res) => {
  var os = require('os');
  var hostname = os.hostname();
  console.log('Hello from ' + hostname);
  req.hostname;
  res.render('auth');
};

exports.postAuth = (req, res, next) => {
  const username = req.body.username;
  dbMaria
    .execute('SELECT * FROM users')
    .then(statusMaria => {
      var userExists = false;
      statusMaria[0].forEach(user => {
        if (user.name == username) {
          userExists = true;
        }
        console.log(user);
      });

      if (userExists) {
        return res.redirect('/dashboard/' + username);
      } else {
        return res.redirect('/');
      }
    })

    .catch(next, error => {
      console.log(new Error(error));
      return res.redirect('/');
    });
};
